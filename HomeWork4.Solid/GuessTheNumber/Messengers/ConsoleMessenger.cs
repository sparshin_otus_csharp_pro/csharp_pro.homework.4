﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Messengers
{
    internal class ConsoleMessenger : IMessenger
    {
        public void SendMessage(string message) => Console.WriteLine(message);
    }
}
