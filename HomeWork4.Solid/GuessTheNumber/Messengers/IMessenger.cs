﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Messengers
{
    public interface IMessenger
    {
        public void SendMessage(string message);
    }
}
