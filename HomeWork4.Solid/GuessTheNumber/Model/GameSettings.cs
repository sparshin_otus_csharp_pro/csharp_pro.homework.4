﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Model
{
    public class GameSettings
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int NumberOfTries { get; set; }
    }
}
