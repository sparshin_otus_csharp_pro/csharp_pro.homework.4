﻿using Microsoft.Extensions.Configuration;
using GuessTheNumber.Messengers;
using GuessTheNumber.Model;
using GuessTheNumber.Actions;
using GuessTheNumber.Generators;

namespace GuessTheNumber
{
    public class Program
    {
        static IMessenger _messenger;
        static GameSettings _gameSettings;

        static void Main(string[] args)
        {
            _messenger = new ConsoleMessenger();

            try
            {
                
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile($"gamesettings.json", optional: true, reloadOnChange: true);

                var config = configuration.Build();

                _gameSettings = config.GetSection("GameSettings").Get<GameSettings>();

                StartGame();
                Console.ReadKey();
            }
            catch(Exception ex)
            {
                _messenger.SendMessage(ex.ToString());
            }

        }

        public static void StartGame()
        {
            _messenger.SendMessage($"Приветствую! Я хочу сыграть с тобой в одну игру!");
            _messenger.SendMessage($"Я загадал целое число в диапазоне от {_gameSettings.MinValue} до {_gameSettings.MaxValue}.");
            _messenger.SendMessage($"У тебя есть {_gameSettings.NumberOfTries} попыток его угадать.");
            _messenger.SendMessage($"Игра началась!");

            int targetValue = new RandomNumberGenerator(_gameSettings).GenerateNumber();
            var valueChecker = new ValueChecker();

            int triesCount = 0;
            do
            {
                
                _messenger.SendMessage("Введи свое число!");
                if (int.TryParse(Console.ReadLine(), out int newValue))
                {
                    if (valueChecker.IsMore(newValue, targetValue))
                    {
                        _messenger.SendMessage("Твое число больше!");
                    }
                    else if (valueChecker.IsLess(newValue, targetValue))
                    {
                        _messenger.SendMessage("Твое число меньше!");
                    }
                    else
                    {
                        _messenger.SendMessage("Ты победил! Удачи там в делах и хорошего настроения!");
                        return;
                    }
                    triesCount++;
                }
                else
                {
                    _messenger.SendMessage("Хорошая попытка, но это не целое число!");
                }
            }
            while (triesCount < _gameSettings.NumberOfTries);

            _messenger.SendMessage($"Увы... Загадано было число {targetValue}. Ну да не страшно, повезет в другой раз");
        }

    }
}