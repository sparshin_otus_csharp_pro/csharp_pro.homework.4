﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Actions
{
    public class ValueChecker: ICheckLess, ICheckMore
    {
        public bool IsLess(int newValue, int targetValue) => newValue < targetValue;
        
        public bool IsMore(int newValue, int targetValue) => newValue > targetValue;
    }
}
