﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Actions
{
    public interface ICheckLess
    {
        public bool IsLess(int newValue, int targetValue);
    }
}
