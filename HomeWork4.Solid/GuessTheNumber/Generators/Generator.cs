﻿using GuessTheNumber.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Generators
{
    public abstract class Generator
    {
        private readonly GameSettings _gameSettings;

        public Generator(GameSettings gameSettings)
        {
            _gameSettings = gameSettings;
        }
    }
}
