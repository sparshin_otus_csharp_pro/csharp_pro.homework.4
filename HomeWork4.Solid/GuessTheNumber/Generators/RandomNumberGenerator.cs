﻿using GuessTheNumber.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Generators
{
    internal class RandomNumberGenerator : Generator, INumberGenerator
    {
        private readonly GameSettings _gameSettings;
        public RandomNumberGenerator(GameSettings gameSettings) : base(gameSettings)
        {
            _gameSettings = gameSettings;
        }

        public int GenerateNumber() => new Random().Next(_gameSettings.MinValue, _gameSettings.MaxValue);
    }
}
