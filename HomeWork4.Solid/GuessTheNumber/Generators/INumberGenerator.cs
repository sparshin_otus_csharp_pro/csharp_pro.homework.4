﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessTheNumber.Generators
{
    public interface INumberGenerator
    {
        public int GenerateNumber();
    }
}
